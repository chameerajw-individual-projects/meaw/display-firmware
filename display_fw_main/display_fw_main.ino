#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0
#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

#include <SoftwareSerial.h>;
#include <ArduinoJson.h>


#include <SPI.h>          // f.k. for Arduino-1.5.2
#include "Adafruit_GFX.h"// Hardware-specific library
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;

SoftwareSerial DisplaySerial(10, 11);

//display size is 240 x 320

String estate_name_array[5] = {"Temp Estate 01", "Temp estate 02", "Temp estate 03", "Temp estate 04", "Temp estate 05"};

#define	BLACK       0x0000
#define	BLUE        0x001F
//#define BLUE        0x565D
#define LIGHT_BLUE  0x565D
#define ORANGE      0xF7D5
#define	RED         0xF800
//#define	GREEN       0x03E0
#define GREEN       0x1DC0
#define CYAN        0x07FF
#define MAGENTA     0xF81F
//#define YELLOW      0xFFE0
#define YELLOW      0xE721
#define WHITE       0xDEBB
#define DARK_GREY   0x7BEF
#define PINK        0xFAB9

#define SIZE2PIXEL 12
#define SIZE3PIXEL 18

#define BATTERY_VOLTAGE_PIN A5

typedef enum {INITIAL, INTERNET, SENDORDELETE, CONFIRMDELETE, DELETESTATE, SELECTESTATE, UPDATEWEIGHT, CONFIRMSEND, SENDSTATE, BATTERY, SIGNAL, TIME, DEVICEID, PACKETCOUNT} displayState_t;

typedef enum {FAILCONNECT, CONNECTED, CONNECT} internet_t;
typedef enum {SEND, DELETE} sendOrDelete_t;
typedef enum {DELETEYES, DELETENO} confirmDelete_t;
typedef enum {DELETING, DELETED} deleteState_t;
typedef enum {LOCK, TARE} updateWeight_t;
typedef enum {SENDYES, SENDNO} confirmSend_t;
typedef enum {SENDING, SENT, SAVED} sendState_t;

int packet_mode = INITIAL;
displayState_t current_display = INITIAL;

uint16_t ID = 0x9341;
//uint16_t scrollbuf[320];    // Adafruit only does 240x320

void setup(void) {
  Serial.begin(115200);
  DisplaySerial.begin(4800);

  initDisplay();

  pinMode(BATTERY_VOLTAGE_PIN, INPUT);

}

void loop(void) {
  if (DisplaySerial.available())
  {
    // Allocate the JSON document
    // This one must be bigger than for the sender because it must store the strings
    StaticJsonDocument<500> doc;

    // Read the JSON document from the "link" serial port
    DeserializationError err = deserializeJson(doc, DisplaySerial);

    if (err == DeserializationError::Ok)
    {
      packet_mode = doc["displayCommand"].as<int>();
      String estate = "qwe";
      String weight = "qwe";
      String timestamp = "qwe";
      /*internet_t internet_state;
      sendOrDelete_t send_or_delete;
      confirmDelete_t confirm_delete;
      deleteState_t delete_state;
      updateWeight_t update_weight;
      confirmSend_t confirm_send;
      sendState_t send_state;*/
      int internet_state, send_or_delete, confirm_delete, delete_state, update_weight, confirm_send, send_state = 0;     
      bool connectivity;
      uint8_t battery_percentage, hours, mins, total, sent, saved = 0;
      uint16_t device_id;

      Serial.println(packet_mode);
      switch (packet_mode)
      {
        case INITIAL:
          //Serial.println("Here 0");
           
          break;
        case INTERNET:
          //Serial.println("Here 1");
          internet_state = doc["state"].as<int>();
          disp_connectingToInternet(internet_state);
          break;
        case SENDORDELETE:
          //Serial.println("Here 2");
          send_or_delete = doc["state"].as<int>();
          disp_sendOrDelete(send_or_delete);
          break;
        case CONFIRMDELETE:
          //Serial.println("Here 3");
          confirm_delete = doc["action"].as<int>();
          estate = doc["estate"].as<String>();
          weight = doc["weight"].as<String>();
          timestamp = doc["timestamp"].as<String>();
          //disp_deleteConfirmation(confirm_delete, estate, weight, timestamp);
          break;
        case DELETESTATE:
          //Serial.println("Here 4");
          delete_state = doc["action"].as<int>();
          estate = doc["estate"].as<String>();
          weight = doc["weight"].as<String>();
          timestamp = doc["timestamp"].as<String>();
          //disp_deleteState(delete_state, estate, weight, timestamp);
          break;
        case SELECTESTATE:
          //Serial.println("Here 5");
          estate = doc["estate"].as<String>();
          //disp_selectEstate(estate);
          break;
        case UPDATEWEIGHT:
          //Serial.println("Here 6");
          update_weight = doc["action"].as<int>();
          weight = doc["weight"].as<String>();
          disp_updateWeight(update_weight, weight);
          break;
        case CONFIRMSEND:
          //Serial.println("Here 7");
          confirm_send = doc["action"].as<int>();
          estate = doc["estate"].as<String>();
          weight = doc["weight"].as<String>();
          timestamp = doc["timestamp"].as<String>();
          disp_sendConfirmation(confirm_send, estate, weight, timestamp);
          break;
        case SENDSTATE:
          //Serial.println("Here 8");
          send_state = doc["state"].as<int>();
          estate = doc["estate"].as<String>();
          weight = doc["weight"].as<String>();
          timestamp = doc["timestamp"].as<String>();
          disp_sendState(send_state, estate, weight, timestamp);
          break;
        case BATTERY:
          //Serial.println("Here 9");
          battery_percentage = doc["battery"].as<uint8_t>();
          disp_updateBattery(battery_percentage);
          break;
        case SIGNAL:
          //Serial.println("Here 10");
          connectivity = doc["state"].as<boolean>();
          disp_updateNetwork(connectivity);
          break;
        case TIME:
          //Serial.println("Here 11");
          hours = doc["hours"].as<uint8_t>();
          mins = doc["hours"].as<uint8_t>();
          disp_updateTime(hours, mins);
          break;
        case DEVICEID:
          //Serial.println("Here 12");
          device_id = doc["device_id"].as<uint16_t>();
          disp_updateBoardID(device_id);
          break;
        case PACKETCOUNT:
          //Serial.println("Here 13");
          total = doc["total"].as<int>();
          sent = doc["sent"].as<int>();
          saved = doc["saved"].as<int>();
          disp_updatePacketCount(total, sent, saved);
          break;
        default:
          //Serial.println("Here default");
          break;
      }
    }
    else
    {
      // Print error to the "debug" serial port
      Serial.print("deserializeJson() returned ");
      Serial.println(err.c_str());

      // Flush all bytes in the "link" serial port buffer
      while (DisplaySerial.available() > 0)
        DisplaySerial.read();
    }
  }
}

/*
  bool com_getSignalState(void)
  {
  StaticJsonDocument<200> doc_send;
  doc_send["command"] = C_INTERNET;
  serializeJson(doc_send, DisplaySerial);
  while (true)
  {
    if (DisplaySerial.available())
    {
      StaticJsonDocument<300> doc_rec;
      DeserializationError err = deserializeJson(doc_rec, DisplaySerial);

      if (err == DeserializationError::Ok)
      {
        bool rec_state = doc_rec["state"].as<bool>();
        return rec_state;
      }
      break;
    }
  }
  return false;
  }

  bool com_sendPacket(String estate, String weight, String timestamp)
  {
  StaticJsonDocument<300> doc_send;
  doc_send["command"] = C_SENDPACKET;
  doc_send["estate"] = estate;
  doc_send["weight"] = weight;
  doc_send["timestamp"] = timestamp;
  serializeJson(doc_send, DisplaySerial);
  while (true)
  {
    if (DisplaySerial.available())
    {
      StaticJsonDocument<300> doc_rec;
      DeserializationError err = deserializeJson(doc_rec, DisplaySerial);

      if (err == DeserializationError::Ok)
      {
        bool rec_state = doc_rec["state"].as<bool>();
        return rec_state;
      }
      break;
    }
  }
  return false;
  }*/
