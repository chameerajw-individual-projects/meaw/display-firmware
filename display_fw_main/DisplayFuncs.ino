void initDisplay(void)
{
  tft.begin(ID);
  //tft.reset(); //hardware reset
  //delay(1000);

  tft.setRotation(3);

  tft.fillScreen(BLACK);

  tft.drawFastHLine(0, 30, 320, CYAN);
  tft.drawFastVLine(242, 0, 30, CYAN);
  tft.drawFastVLine(183, 0, 30, CYAN);
  tft.drawFastVLine(106, 0, 30, CYAN);

  tft.drawFastHLine(0, 60, 320, CYAN);
  tft.drawFastVLine(106, 30, 30, CYAN);
  tft.drawFastVLine(213, 30, 30, CYAN);

  disp_displayPacketHeaders();
}

void disp_displayPacketHeaders(void)
{
  tft.setTextColor(WHITE, BLACK);
  tft.setTextSize(1);
  tft.setCursor(5, 43);
  tft.write("TOTAL");
  tft.setCursor(112, 43);
  tft.write("SENT");
  tft.setCursor(219, 43);
  tft.write("SAVED");
}

void disp_updateBattery(uint8_t percentage)
{
  if (percentage < 30)
  {
    //tft.setTextColor(RED, BLACK); // text, background
    tft.fillRect(254, 7 , 20, 15, RED);
    tft.fillRect(249, 11 , 5, 7, RED);
  }
  else
  {
    //tft.setTextColor(WHITE, BLACK);
    tft.fillRect(254, 7 , 20, 15, GREEN);
    tft.fillRect(249, 11 , 5, 7, GREEN);
  }

  tft.drawLine(257, 22, 264, 7, BLACK);
  tft.drawLine(265, 22, 272, 7, BLACK);

  char percentageChar[sizeof(int) * 8 + 1] = {0};
  itoa((int)percentage, percentageChar, DEC);

  if (percentage < 10)
  {
    percentageChar[1] = percentageChar[0];
    percentageChar[0] = '0';
  }

  percentageChar[2] = '%';
  percentageChar[3] = '\0';

  Serial.print("Battery Percentage :  ");
  Serial.println(percentageChar);

  tft.setCursor(281, 7);
  tft.setTextColor(WHITE, BLACK);
  tft.setTextSize(2);
  tft.write(percentageChar);
}

void disp_updateNetwork(bool strength)
{
  int startPos = 190;
  if (strength == true)
  {
    tft.fillTriangle(startPos, 20, startPos + 18, 20, startPos + 18, 7, GREEN);
    tft.fillRect(startPos + 20, 0 , 29, 30, BLACK);
    tft.drawLine(startPos + 28, 18, startPos + 28 + 13, 8, WHITE);
    tft.drawLine(startPos + 29, 18, startPos + 29 + 13, 8, WHITE);
    tft.drawLine(startPos + 28, 18, startPos + 28 - 5, 13, WHITE);
    tft.drawLine(startPos + 29, 18, startPos + 29 - 5, 13, WHITE);
  }
  else
  {
    tft.fillTriangle(startPos, 20, startPos + 18, 20, startPos + 18, 7, RED);
    tft.fillRect(startPos + 20, 0 , 29, 30, BLACK);
    tft.drawLine(startPos + 26, 18, startPos + 26 + 13, 8, WHITE);
    tft.drawLine(startPos + 27, 18, startPos + 27 + 13, 8, WHITE);
    tft.drawLine(startPos + 26, 8, startPos + 26 + 13, 18, WHITE);
    tft.drawLine(startPos + 27, 8, startPos + 27 + 13, 18, WHITE);
  }
}

void disp_updateTime(uint8_t hours, uint8_t mins)
{
  char hoursChar[sizeof(int) * 8 + 1] = {0};
  itoa((int)hours, hoursChar, DEC);

  char minsChar[sizeof(int) * 8 + 1] = {0};
  itoa((int)mins, minsChar, DEC);

  char timeChar [6] = {0};

  if (hours < 10)
  {
    timeChar[0] = '0';
    timeChar[1] = hoursChar[0];
  }
  else
  {
    timeChar[0] = hoursChar[0];
    timeChar[1] = hoursChar[1];
  }

  timeChar[2] = ':';

  if (mins < 10)
  {
    timeChar[3] = '0';
    timeChar[4] = minsChar[0];
  }
  else
  {
    timeChar[3] = minsChar[0];
    timeChar[4] = minsChar[1];
  }

  timeChar[5] = '\0';

  Serial.print("Current Time :  ");
  Serial.println(timeChar);

  tft.setCursor(115, 7);
  tft.setTextColor(WHITE, BLACK);
  tft.setTextSize(2);
  tft.write(timeChar);
}

void disp_updateBoardID(uint16_t boardID)
{
  char boardIDChar[sizeof(int) * 8 + 1] = {0};
  itoa((int)boardID, boardIDChar, DEC);

  char boardIDPrint[8] = {0};

  boardIDPrint[0] = 'I';
  boardIDPrint[1] = 'D';
  boardIDPrint[2] = ':';

  boardIDPrint[3] = boardIDChar[0];
  boardIDPrint[4] = boardIDChar[1];
  boardIDPrint[5] = boardIDChar[2];
  boardIDPrint[6] = boardIDChar[3];

  boardIDPrint[7] = '\0';

  Serial.print("Board ID :  ");
  Serial.println(boardIDPrint);

  tft.setCursor(7, 7);
  tft.setTextColor(WHITE, BLACK);
  tft.setTextSize(2);
  tft.write(boardIDPrint);
}

void disp_updatePacketCount(uint8_t total, uint8_t sent, uint8_t saved)
{
  char totalChar[sizeof(int) * 8 + 1] = {0};
  itoa((int)total, totalChar, DEC);

  char sentChar[sizeof(int) * 8 + 1] = {0};
  itoa((int)sent, sentChar, DEC);

  char savedChar[sizeof(int) * 8 + 1] = {0};
  itoa((int)saved, savedChar, DEC);

  tft.setTextSize(2);

  tft.setTextColor(YELLOW, BLACK);
  tft.setCursor(55, 38);
  tft.write(totalChar);

  tft.setTextColor(GREEN, BLACK);
  tft.setCursor(112 + 40, 38);
  tft.write(sentChar);

  tft.setTextColor(RED, BLACK);
  tft.setCursor(219 + 45, 38);
  tft.write(savedChar);
}

void disp_connectingToInternet(internet_t state)
{
  int startY = 90;
  if (current_display != INTERNET)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.setTextSize(3);

    tft.setTextColor(WHITE, BLACK);
    tft.setCursor(40, startY);
    tft.write("CONNECTING TO");
    tft.setCursor(50, startY + 35);
    tft.write("THE INTERNET");
  }
  current_display = INTERNET;

  tft.fillRect(0, startY + 75 , 340, 240 - (startY + 75), BLACK); // clear WAIT, SUCCESS, FAIL

  tft.setTextSize(4);
  if (state == CONNECT)
  {
    tft.setTextColor(YELLOW, BLACK);
    tft.setCursor(80, startY + 80);
    tft.write("WAIT...");
  }
  else if (state == CONNECTED)
  {
    tft.setTextColor(GREEN, BLACK);
    tft.setCursor(55, startY + 80);
    tft.write("SUCCESS !");
  }
  else if (state == FAILCONNECT)
  {
    tft.setTextColor(RED, BLACK);
    tft.setCursor(70, startY + 80);
    tft.write("FAIL !!!");
  }
}

void disp_sendOrDelete(sendOrDelete_t action)
{
  int startY = 70;
  tft.setTextSize(5);

  if (current_display != SENDORDELETE)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.fillRoundRect(50, startY + 15, 220, 50, 15, GREEN);
    tft.setTextColor(WHITE, GREEN);
    tft.setCursor(103, startY + 22);
    tft.write("SEND");

    tft.fillRoundRect(50, startY + 100, 220, 50, 15, RED);
    tft.setTextColor(WHITE, RED);
    tft.setCursor(72, startY + 107);
    tft.write("DELETE");
  }

  current_display = SENDORDELETE;

  if (action == SEND)
  {
    tft.drawRoundRect(40, startY + 5, 240, 70, 15, WHITE);
    tft.drawRoundRect(41, startY + 6, 238, 68, 15, WHITE);
    tft.drawRoundRect(40, startY + 90, 240, 70, 15, BLACK);
    tft.drawRoundRect(41, startY + 91, 238, 68, 15, BLACK);
  }
  else if (action == DELETE)
  {
    tft.drawRoundRect(40, startY + 90, 240, 70, 15, WHITE);
    tft.drawRoundRect(41, startY + 91, 238, 68, 15, WHITE);
    tft.drawRoundRect(40, startY + 5, 240, 70, 15, BLACK);
    tft.drawRoundRect(41, startY + 6, 238, 68, 15, BLACK);
  }
}

void disp_deleteConfirmation(confirmDelete_t action, String estate, String weight, String timeFrame)
{
  int startY = 70;
  if (current_display != CONFIRMDELETE)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.setTextSize(3);
    tft.setTextColor(WHITE, BLACK);
    tft.setCursor(25, startY);
    tft.write("CONFIRM DELETE?");

    int stateLength = estate.length();
    char estateChar [stateLength + 1] = {0};
    estate.toCharArray(estateChar, stateLength + 1);
    tft.setTextSize(2);
    tft.setTextColor(ORANGE, BLACK);
    int estateStartX = max(0, ((320 - (SIZE2PIXEL * stateLength)) / 2)); //if string is longer than the screen
    Serial.println(estateStartX);
    tft.setCursor(estateStartX, startY + 30);
    tft.write(estateChar);

    int weightLength = weight.length();
    char weightChar[weightLength + 1] = {0};
    weight.toCharArray(weightChar, weightLength + 1);
    tft.setTextColor(LIGHT_BLUE, BLACK);
    tft.setCursor(110, startY + 55);
    tft.write(weightChar);
    tft.setCursor(190, startY + 55);
    tft.write("KG");

    int timeFrameLength = timeFrame.length();
    char timeFrameChar[timeFrameLength + 1] = {0};
    timeFrame.toCharArray(timeFrameChar, timeFrameLength + 1);
    tft.setTextColor(PINK, BLACK);
    tft.setCursor(115, startY + 80);
    tft.write(timeFrameChar);

    tft.setTextSize(4);

    //NO BUTTON
    tft.fillRoundRect(30, startY + 110, 110, 50, 15, GREEN);
    tft.setTextColor(WHITE, GREEN);
    tft.setCursor(65, startY + 120);
    tft.write("NO");

    //YES BUTTON
    tft.fillRoundRect(175, startY + 110, 110, 50, 15, RED);
    tft.setTextColor(WHITE, RED);
    tft.setCursor(197, startY + 120);
    tft.write("YES");
  }
  current_display = CONFIRMDELETE;

  if (action == DELETENO)
  {
    tft.drawRoundRect(20, startY + 100, 130, 70, 15, WHITE);
    tft.drawRoundRect(21, startY + 101, 128, 68, 15, WHITE);
    tft.drawRoundRect(165, startY + 100, 130, 70, 15, BLACK);
    tft.drawRoundRect(166, startY + 101, 128, 68, 15, BLACK);
  }
  else if (action == DELETEYES)
  {
    tft.drawRoundRect(165, startY + 100, 130, 70, 15, WHITE);
    tft.drawRoundRect(166, startY + 101, 128, 68, 15, WHITE);
    tft.drawRoundRect(20, startY + 100, 130, 70, 15, BLACK);
    tft.drawRoundRect(21, startY + 101, 128, 68, 15, BLACK);
  }
}

void disp_deleteState(deleteState_t state, String estate, String weight, String timeFrame)
{
  int startY = 90;
  if (current_display != DELETESTATE)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.setTextSize(3);

    tft.setTextColor(WHITE, BLACK);
    tft.setCursor(62, startY - 10);
    tft.write("DELETING..!");

    int stateLength = estate.length();
    char estateChar [stateLength + 1] = {0};
    estate.toCharArray(estateChar, stateLength + 1);
    tft.setTextSize(2);
    tft.setTextColor(ORANGE, BLACK);
    int estateStartX = max(0, ((320 - (SIZE2PIXEL * stateLength)) / 2)); //if string is longer than the screen
    Serial.println(estateStartX);
    tft.setCursor(estateStartX, startY + 25);
    tft.write(estateChar);

    int weightLength = weight.length();
    char weightChar[weightLength + 1] = {0};
    weight.toCharArray(weightChar, weightLength + 1);
    tft.setTextColor(LIGHT_BLUE, BLACK);
    tft.setCursor(110, startY + 50);
    tft.write(weightChar);
    tft.setCursor(190, startY + 50);
    tft.write("KG");

    int timeFrameLength = timeFrame.length();
    char timeFrameChar[timeFrameLength + 1] = {0};
    timeFrame.toCharArray(timeFrameChar, timeFrameLength + 1);
    tft.setTextColor(PINK, BLACK);
    tft.setCursor(115, startY + 75);
    tft.write(timeFrameChar);
  }
  current_display = DELETESTATE;

  tft.fillRect(0, startY + 100 , 340, 240 - (startY + 75), BLACK); // clear WAIT, SUCCESS

  tft.setTextSize(4);
  if (state == DELETING)
  {
    tft.setTextColor(YELLOW, BLACK);
    tft.setCursor(80, startY + 110);
    tft.write("WAIT...");
  }
  else if (state == DELETED)
  {
    tft.setTextColor(GREEN, BLACK);
    tft.setCursor(55, startY + 110);
    tft.write("SUCCESS !");
  }
}

void disp_selectEstate(String estate)
{
  int startY = 70;
  if (current_display != SELECTESTATE)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.setTextSize(3);
    tft.setTextColor(WHITE, BLACK);
    tft.setCursor(45, startY);
    tft.write("SELECT ESTATE");

    //CANCEL BUTTON
    tft.setTextSize(3);
    tft.fillRoundRect(200, 190, 120, 50, 15, RED);
    tft.setTextColor(WHITE, RED);
    tft.setCursor(210, 205);
    tft.write("CANCEL");

    //SELECT BUTTON
    tft.setTextSize(3);
    tft.fillRoundRect(0, 190, 120, 50, 15, GREEN);
    tft.setTextColor(WHITE, GREEN);
    tft.setCursor(5, 205);
    tft.write("SELECT");

    //UP ARROW
    tft.fillRect(175, 240 - 35, 10, 35, BLUE);
    tft.fillTriangle(180 - 12, 240 - 35, 180 + 11, 240 - 35, 180, 190, BLUE);

    //DOWN ARROW
    tft.fillRect(135, 240 - 48, 10, 35, BLUE);
    tft.fillTriangle(140 - 12, 240 - 13, 140 + 11, 240 - 13, 140, 239, BLUE);
  }
  current_display = SELECTESTATE;

  tft.fillRect(0, 90 , 340, 80, BLACK);

  int stateLength = estate.length();
  char estateChar [stateLength + 1] = {0};
  estate.toCharArray(estateChar, stateLength + 1);
  tft.setTextColor(ORANGE, BLACK);

  int estateStartX = 0;
  if (stateLength <= 17)
  {
    estateStartX = max(0, ((320 - (SIZE3PIXEL * stateLength)) / 2)); //if string is longer than the screen
    tft.setTextSize(3);
  }
  else
  {
    estateStartX = max(0, ((320 - (SIZE2PIXEL * stateLength)) / 2)); //if string is longer than the screen
    tft.setTextSize(2);
  }
  tft.setCursor(estateStartX, startY + 50);
  tft.write(estateChar);
}

void disp_updateWeight(updateWeight_t action, String weight)
{
  int startY = 70;
  if (current_display != UPDATEWEIGHT)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.setTextSize(3);
    tft.setTextColor(WHITE, BLACK);
    tft.setCursor(45, startY);
    tft.write("SELECT WEIGHT");


    //LOCK BUTTON
    tft.fillRoundRect(30, startY + 105, 110, 45, 15, GREEN);
    tft.setTextColor(WHITE, GREEN);
    tft.setCursor(50, startY + 118);
    tft.write("LOCK");

    //TARE BUTTON
    tft.fillRoundRect(175, startY + 105, 110, 45, 15, RED);
    tft.setTextColor(WHITE, RED);
    tft.setCursor(197, startY + 118);
    tft.write("TARE");
  }
  current_display = UPDATEWEIGHT;

  tft.fillRect(0, 95 , 320, 70, BLACK);

  int weightLength = weight.length();
  char weightChar[weightLength + 1] = {0};
  weight.toCharArray(weightChar, weightLength + 1);

  //tft.setFont(&Picopixel);
  tft.setCursor(30, startY + 40);
  tft.setTextSize(5);
  tft.setTextColor(ORANGE, BLACK);
  tft.write(weightChar);
  tft.setCursor(230, startY + 40);
  tft.write("KG");
  //tft.setFont();

  if (action == LOCK)
  {
    tft.drawRoundRect(20, startY + 95, 130, 65, 15, WHITE);
    tft.drawRoundRect(21, startY + 96, 128, 63, 15, WHITE);
    tft.drawRoundRect(165, startY + 95, 130, 65, 15, BLACK);
    tft.drawRoundRect(166, startY + 96, 128, 63, 15, BLACK);
  }
  else if (action == TARE)
  {
    tft.drawRoundRect(165, startY + 95, 130, 65, 15, WHITE);
    tft.drawRoundRect(166, startY + 96, 128, 63, 15, WHITE);
    tft.drawRoundRect(20, startY + 95, 130, 65, 15, BLACK);
    tft.drawRoundRect(21, startY + 96, 128, 63, 15, BLACK);
  }
}

void disp_sendConfirmation(confirmSend_t action, String estate, String weight, String timeFrame)
{
  int startY = 70;
  if (current_display != CONFIRMSEND)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.setTextSize(3);
    tft.setTextColor(WHITE, BLACK);
    tft.setCursor(45, startY);
    tft.write("CONFIRM SEND?");

    int stateLength = estate.length();
    char estateChar [stateLength + 1] = {0};
    estate.toCharArray(estateChar, stateLength + 1);
    tft.setTextSize(2);
    tft.setTextColor(ORANGE, BLACK);
    int estateStartX = max(0, ((320 - (SIZE2PIXEL * stateLength)) / 2)); //if string is longer than the screen
    Serial.println(estateStartX);
    tft.setCursor(estateStartX, startY + 30);
    tft.write(estateChar);

    int weightLength = weight.length();
    char weightChar[weightLength + 1] = {0};
    weight.toCharArray(weightChar, weightLength + 1);
    tft.setTextColor(LIGHT_BLUE, BLACK);
    tft.setCursor(110, startY + 55);
    tft.write(weightChar);
    tft.setCursor(190, startY + 55);
    tft.write("KG");

    int timeFrameLength = timeFrame.length();
    char timeFrameChar[timeFrameLength + 1] = {0};
    timeFrame.toCharArray(timeFrameChar, timeFrameLength + 1);
    tft.setTextColor(PINK, BLACK);
    tft.setCursor(115, startY + 80);
    tft.write(timeFrameChar);

    tft.setTextSize(4);

    //NO BUTTON
    tft.fillRoundRect(30, startY + 110, 110, 50, 15, GREEN);
    tft.setTextColor(WHITE, GREEN);
    tft.setCursor(65, startY + 120);
    tft.write("NO");

    //YES BUTTON
    tft.fillRoundRect(175, startY + 110, 110, 50, 15, RED);
    tft.setTextColor(WHITE, RED);
    tft.setCursor(197, startY + 120);
    tft.write("YES");
  }
  current_display = CONFIRMSEND;

  if (action == SENDNO)
  {
    tft.drawRoundRect(20, startY + 100, 130, 70, 15, WHITE);
    tft.drawRoundRect(21, startY + 101, 128, 68, 15, WHITE);
    tft.drawRoundRect(165, startY + 100, 130, 70, 15, BLACK);
    tft.drawRoundRect(166, startY + 101, 128, 68, 15, BLACK);
  }
  else if (action == SENDYES)
  {
    tft.drawRoundRect(165, startY + 100, 130, 70, 15, WHITE);
    tft.drawRoundRect(166, startY + 101, 128, 68, 15, WHITE);
    tft.drawRoundRect(20, startY + 100, 130, 70, 15, BLACK);
    tft.drawRoundRect(21, startY + 101, 128, 68, 15, BLACK);
  }
}

void disp_sendState(sendState_t state, String estate, String weight, String timeFrame)
{
  int startY = 90;
  if (current_display != SENDSTATE)
  {
    tft.fillRect(0, 61 , 340, 240 - 61, BLACK);

    tft.setTextSize(3);

    tft.setTextColor(WHITE, BLACK);
    tft.setCursor(72, startY - 10);
    tft.write("SENDING..!");

    int stateLength = estate.length();
    char estateChar [stateLength + 1] = {0};
    estate.toCharArray(estateChar, stateLength + 1);
    tft.setTextSize(2);
    tft.setTextColor(ORANGE, BLACK);
    int estateStartX = max(0, ((320 - (SIZE2PIXEL * stateLength)) / 2)); //if string is longer than the screen
    Serial.println(estateStartX);
    tft.setCursor(estateStartX, startY + 25);
    tft.write(estateChar);

    int weightLength = weight.length();
    char weightChar[weightLength + 1] = {0};
    weight.toCharArray(weightChar, weightLength + 1);
    tft.setTextColor(LIGHT_BLUE, BLACK);
    tft.setCursor(110, startY + 50);
    tft.write(weightChar);
    tft.setCursor(190, startY + 50);
    tft.write("KG");

    int timeFrameLength = timeFrame.length();
    char timeFrameChar[timeFrameLength + 1] = {0};
    timeFrame.toCharArray(timeFrameChar, timeFrameLength + 1);
    tft.setTextColor(PINK, BLACK);
    tft.setCursor(115, startY + 75);
    tft.write(timeFrameChar);
  }
  current_display = SENDSTATE;

  tft.fillRect(0, startY + 100 , 340, 240 - (startY + 75), BLACK); // clear WAIT, SUCCESS

  tft.setTextSize(4);
  if (state == SENDING)
  {
    tft.setTextColor(YELLOW, BLACK);
    tft.setCursor(80, startY + 115);
    tft.write("WAIT...");
  }
  else if (state == SENT)
  {
    tft.setTextColor(GREEN, BLACK);
    tft.setCursor(55, startY + 115);
    tft.write("SUCCESS !");
  }

  else if (state == SAVED)
  {
    tft.setTextColor(RED, BLACK);
    tft.setCursor(70, startY + 115);
    tft.write("SAVED !");
  }
}

void testFunc(void)
{
  disp_updateBattery(91);
  disp_updateNetwork(false);
  disp_updateTime(1, 30);
  disp_updateBoardID(5634);
  disp_updatePacketCount(86, 38, 48);

  Serial.println(current_display);
  disp_connectingToInternet(CONNECT);
  delay(1000);
  disp_connectingToInternet(CONNECTED);
  delay(1000);
  disp_connectingToInternet(FAILCONNECT);
  delay(1000);

  Serial.println(current_display);
  disp_sendOrDelete(SEND);
  delay(1000);
  disp_sendOrDelete(DELETE);
  delay(1000);

  Serial.println(current_display);
  disp_deleteConfirmation(DELETEYES, "TEMPORARY ESTATE 01", "25.525", "01:45 PM");
  delay(1000);
  disp_deleteConfirmation(DELETENO, "TEMPORARY", "11.111", "01:45 PM");
  delay(1000);

  disp_deleteState(DELETING, "TEMPORARY STATE 01", "34.234", "01:45 PM");
  delay(1000);
  disp_deleteState(DELETED,  "STATE 25", "11.101", "88:88 PM");
  delay(1000);

  Serial.println(current_display);
  disp_selectEstate("TEMPORARY ESTATE 01");
  delay(1000);
  disp_selectEstate("TEMPORARY ESTATE 010203040506070809");
  delay(1000);
  disp_selectEstate("TEMPORARY");
  delay(1000);

  disp_updateWeight(LOCK, "23.564");
  delay(1000);
  disp_updateWeight(TARE, "10.064");
  delay(1000);
  disp_updateWeight(LOCK, "11.111");
  delay(1000);

  disp_sendConfirmation(SENDYES, "TEMPORARY ESTATE 01", "25.525", "01:45 PM");
  delay(1000);
  disp_sendConfirmation(SENDNO, "TEMPORARY", "11.111", "01:45 PM");
  delay(1000);

  disp_sendState(SENDING, "TEMPORARY STATE 01", "34.234", "12:58 PM");
  delay(1000);
  disp_sendState(SENT,  "STATE 25", "11.101", "88:88 PM");
  delay(1000);
  disp_sendState(SAVED,  "STATE 25", "11.101", "88:88 PM");

  disp_updateBattery(12);
  disp_updateNetwork(true);
  disp_updateTime(8, 11);
  disp_updateBoardID(1111);
  disp_updatePacketCount(10, 9, 1);
}
