/*if (DisplaySerial.available())
{
  // Allocate the JSON document
  // This one must be bigger than for the sender because it must store the strings
  StaticJsonDocument<300> doc;

  // Read the JSON document from the "link" serial port
  DeserializationError err = deserializeJson(doc, DisplaySerial);

  if (err == DeserializationError::Ok)
  {
    packet_mode = doc["commonDisplayState"].as<int>();
    String estate = " ";
    String weight = " ";
    String timestamp = " ";

    switch (packet_mode)
    {
      case INITIAL:
        break;
      case INTERNET:
        internet_t internet_state = doc["action"].as<int>();
        connectingToInternet(internet_state);
        break;
      case SENDORDELETE:
        sendOrDelete_t send_or_delete = doc["action"].as<int>();
        sendOrDelete(send_or_delete);
        break;
      case CONFIRMDELETE:
        confirmDelete_t confirm_delete = doc["action"].as<int>();
        estate = doc["estate"].as<String>();
        weight = doc["weight"].as<String>();
        timestamp = doc["timestamp"].as<String>();
        deleteConfirmation(confirm_delete, estate, weight, timestamp);
        break;
      case DELETESTATE:
        deleteState_t delete_state = doc["action"].as<int>();
        estate = doc["estate"].as<String>();
        weight = doc["weight"].as<String>();
        timestamp = doc["timestamp"].as<String>();
        deleteState(delete_state, estate, weight, timestamp);
        break;
      case SELECTESTATE:
        estate = doc["estate"].as<String>();
        selectEstate(estate);
        break;
      case UPDATEWEIGHT:
        updateWeight_t update_weight = doc["action"].as<int>();
        weight = doc["weight"].as<String>();
        updateWeight(update_weight, weight);
        break;
      case CONFIRMSEND:
        confirmSend_t confirm_send = doc["action"].as<int>();
        estate = doc["estate"].as<String>();
        weight = doc["weight"].as<String>();
        timestamp = doc["timestamp"].as<String>();
        sendConfirmation(confirm_send, estate, weight, timestamp);
        break;
      case SENDSTATE:
        sendState_t send_state = doc["action"].as<int>();
        estate = doc["estate"].as<String>();
        weight = doc["weight"].as<String>();
        timestamp = doc["timestamp"].as<String>();
        sendState(send_state, estate, weight, timestamp);
        break;
      case BATTERY:
        uint8_t battery_percentage = doc["battery"].as<uint8_t>();
        updateBattery(battery_percentage);
        break;
      case SIGNAL:
        bool connectivity = doc["connectivity"].as<boolean>();
        updateNetwork(connectivity);
        break;
      case TIME:
        uint8_t hours = doc["hours"].as<uint8_t>();
        uint8_t mins = doc["hours"].as<uint8_t>();
        updateTime(hours, mins);
        break;
      case DEVICEID:
        uint16_t device_id = doc["device_id"].as<uint16_t>();
        updateBoardID(device_id);
        break;
      case PACKETCOUNT:
        uint8_t total = doc["total"].as<uint8_t>();
        uint8_t sent = doc["sent"].as<uint8_t>();
        uint8_t saved = doc["saved"].as<uint8_t>();
        updatePacketCount(total, sent, saved);
        break;
    }
  }
  else
  {
    // Print error to the "debug" serial port
    Serial.print("deserializeJson() returned ");
    Serial.println(err.c_str());

    // Flush all bytes in the "link" serial port buffer
    while (DisplaySerial.available() > 0)
      DisplaySerial.read();
  }
}*/
